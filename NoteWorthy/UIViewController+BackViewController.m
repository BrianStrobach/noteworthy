//
//  UIViewController+BackViewController.m
//  NoteWorthy
//
//  Created by Brian Strobach on 5/23/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import "UIViewController+BackViewController.h"

@implementation UIViewController (BackViewController)

- (UIViewController *)backViewController
{
    NSInteger viewControllerCount = self.navigationController.viewControllers.count;
    
    if (viewControllerCount < 2)
        return nil;
    else
        return [self.navigationController.viewControllers objectAtIndex:viewControllerCount - 2];
}
@end
