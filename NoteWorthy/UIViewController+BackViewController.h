//
//  UIViewController+BackViewController.h
//  NoteWorthy
//
//  Created by Brian Strobach on 5/23/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (BackViewController)
- (UIViewController *)backViewController;
@end
