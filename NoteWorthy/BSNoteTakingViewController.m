//
//  BSViewController.m
//  NoteWorthy
//
//  Created by Brian Strobach on 5/21/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import "BSNoteTakingViewController.h"
#import <Dropbox/Dropbox.h>
#import "BSNote.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <ImageIO/ImageIO.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface BSNoteTakingViewController ()
@property (strong, nonatomic) IBOutlet UITextView *noteTextView;
@property (strong, nonatomic) IBOutlet UIToolbar *noteToolBar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *doneBtn;
@property (strong, nonatomic) IBOutlet UIImageView *noteImageView;
@property (strong, nonatomic) BSNote *note;

@end

@implementation BSNoteTakingViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[DBAccountManager sharedManager] linkFromController:self];
    _noteTextView.delegate = self;
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated{
    if (_note) {
        _noteTextView.text = _note.noteText;
        _noteImageView.image = _note.noteImage;
    }
    else{
        _note = [BSNote new];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cameraButtonPressed:(UIBarButtonItem *)sender {
    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker =
        [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        imagePicker.sourceType =
        UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        
        imagePicker.allowsEditing = YES;
        [self presentViewController:imagePicker
                           animated:YES completion:nil];
    }
}

#pragma mark UIImagePickerControllerDelegate

-(void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image;
        
        image = info[UIImagePickerControllerEditedImage];
        UIImageWriteToSavedPhotosAlbum(image,
                                       self,
                                       @selector(image:finishedSavingWithError:contextInfo:),
                                       nil);
        _note.noteImage = image;
        _noteImageView.image = image;
        
    }
}


-(void)image:(UIImage *)image
finishedSavingWithError:(NSError *)error
 contextInfo:(void *)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle: @"Save failed"
                              message: @"Failed to save image"
                              delegate: nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil];
        [alert show];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void)textViewDidChange:(UITextView *)textView{
    _note.noteText = textView.text;
}


- (IBAction)doneButtonPressed:(id)sender {
    //In case the keyboard is still visible
    [self.view endEditing:YES];
    
    if (_note.noteText || _note.noteImage) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
            [self saveNoteToDB];
            
            if (_note.noteText && _note.noteImage) {
                NSMutableDictionary *tiffMetadata = [[NSMutableDictionary alloc] init];
                [tiffMetadata setObject:_note.noteText forKey:(NSString*)kCGImagePropertyTIFFImageDescription];
                NSMutableDictionary *metadata = [[NSMutableDictionary alloc] init];
                [metadata setObject:tiffMetadata forKey:(NSString*)kCGImagePropertyTIFFDictionary];
                
                ALAssetsLibrary *al = [[ALAssetsLibrary alloc] init];
                
                [al writeImageToSavedPhotosAlbum:[_note.noteImage CGImage]
                                        metadata:metadata
                                 completionBlock:^(NSURL *assetURL, NSError *error) {
                                     if (error == nil) {
                                         NSLog(@"Meta data saved");
                                     } else {
                                         NSLog(@"%@", [error localizedDescription]);
                                     }
                                 }];
                
            }
            _note = [BSNote new];
            dispatch_async(dispatch_get_main_queue(), ^{
                _noteImageView.image = nil;
                _noteTextView.text = nil;
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            });
        });
        
        
    }
    else{
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Note is blank"
                                                     message:@"You must add text or an image before a note can be saved to drop box"
                                                    delegate:nil
                                           cancelButtonTitle:@"OK"
                                           otherButtonTitles:nil];
        [av show];
    }
}

-(void)setNote:(BSNote *)note{
    _note = note;
}

- (void)saveNoteToDB{
    DBFilesystem *fileSystem = [DBFilesystem sharedFilesystem];
    NSString *childPathString = [NSString stringWithFormat:@"%@.note",[[NSDate date] description]];
    DBPath *path = [[DBPath root] childPath:childPathString];
    DBFile *file = [fileSystem createFile:path error:nil];
    NSData *noteData = [NSKeyedArchiver archivedDataWithRootObject:_note];
    [file writeData:noteData error:nil];
    
    NSString *contents = [file readString:nil];
    NSLog(@"%@", contents);
    
}





@end
