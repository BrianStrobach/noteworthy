//
//  BSNoteTableViewController.m
//  NoteWorthy
//
//  Created by Brian Strobach on 5/23/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import "BSNoteTableViewController.h"
#import "BSNoteTakingViewController.h"
#import <Dropbox/Dropbox.h>
#import "BSNote.h"
@interface BSNoteTableViewController ()
@property (strong, nonatomic) NSMutableArray *notes;
@end

@implementation BSNoteTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!_notes) {
        _notes = [NSMutableArray array];
    }
    self.tableView.delegate = self;
    [self fetchNotesFromDB];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
}

- (void)fetchNotesFromDB{
    DBFilesystem *fileSystem = [DBFilesystem sharedFilesystem];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^() {
        NSArray *immContents = [fileSystem listFolder:[DBPath root] error:nil];
        for (DBFileInfo *fileData in immContents) {
            DBError *error;
            DBFile *file = [fileSystem openFile:[fileData path] error:&error];
            if (error) NSLog(@"Error reading file: %@", [error localizedDescription]);
            NSData *noteData = [file readData:&error];
            if (error) NSLog(@"Error reading file: %@", [error localizedDescription]);
            BSNote *note = [NSKeyedUnarchiver unarchiveObjectWithData:noteData];
            [_notes addObject:note];
        }
        dispatch_async(dispatch_get_main_queue(), ^() {
            [self.tableView reloadData];
        });
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_notes count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"noteCell" forIndexPath:indexPath];
    
    // Configure the cell...
    BSNote *note = [_notes objectAtIndex:indexPath.row];
    cell.textLabel.text = note.noteText;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    BSNoteTakingViewController *vc = (BSNoteTakingViewController *)[self backViewController];
    [vc setNote:[_notes objectAtIndex:indexPath.row]];
    [self.navigationController popViewControllerAnimated:YES];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



@end
