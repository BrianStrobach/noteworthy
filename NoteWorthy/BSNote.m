//
//  BSNote.m
//  NoteWorthy
//
//  Created by Brian Strobach on 5/21/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import "BSNote.h"

@implementation BSNote 

- (id)initWithCoder:(NSCoder *)decoder {
    if (self = [super init]) {
        self.noteText = [decoder decodeObjectForKey:@"noteText"];
        self.noteImage = [decoder decodeObjectForKey:@"noteImage"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeObject:_noteText forKey:@"noteText"];
    [encoder encodeObject:_noteImage forKey:@"noteImage"];
}

@end
