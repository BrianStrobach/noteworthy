//
//  main.m
//  NoteWorthy
//
//  Created by Brian Strobach on 5/21/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BSAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BSAppDelegate class]));
    }
}
