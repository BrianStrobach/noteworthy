//
//  BSNote.h
//  NoteWorthy
//
//  Created by Brian Strobach on 5/21/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSNote : NSObject <NSCoding>

@property (strong, nonatomic) NSString *noteText;
@property (strong, nonatomic) UIImage *noteImage;
@end
