//
//  BSViewController.h
//  NoteWorthy
//
//  Created by Brian Strobach on 5/21/14.
//  Copyright (c) 2014 Brian Strobach. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSNote.h"
#import "MBProgressHUD.h"
@interface BSNoteTakingViewController : UIViewController <UITextViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

- (void)setNote:(BSNote *)note;

@end
